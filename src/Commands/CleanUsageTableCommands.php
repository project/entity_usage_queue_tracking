<?php

namespace Drupal\entity_usage_queue_tracking\Commands;

use Drush\Commands\DrushCommands;

/**
 * Exposes the CleanUsageTable service to clean the usage table.
 */
class CleanUsageTableCommands extends DrushCommands {

  private $cleanUsageTableService;

  /**
   * {@inheritdoc}
   */
  public function __construct($clean_usage_table) {
    $this->cleanUsageTableService = $clean_usage_table;
  }

  /**
   * Drush command that clean the usage table.
   *
   * @command clean_usage_table
   * @option pointing Also delete records which point at themselves.
   */
  public function clean($options = ['pointing' => FALSE]) {
    $this->cleanUsageTableService->clean($options);
  }

}
