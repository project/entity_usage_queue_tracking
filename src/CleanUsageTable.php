<?php

namespace Drupal\entity_usage_queue_tracking;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\RevisionableInterface;
use Psr\Log\LoggerInterface;

/**
 * Cleans entity usage table from repeated records.
 */
class CleanUsageTable {

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * Entity Type Manager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  private LoggerInterface $logger;

  /**
   * Returns a CleanUsageTable object with populated properties.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Database\Connection $database
   * @param \Psr\Log\LoggerInterface $logger
   *
   * @return void
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database, LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->logger = $logger;
  }

  /**
   * Execute private methods for cleaning.
   */
  public function clean($options = []) {
    $this->deleteRecordsWithNonCurrentRevisions();
    if (!empty($options['pointing'])) {
      $this->deleteRecordsPointingThemselves();
    }
  }

  /**
   * Delete references from other revisions.
   */
  private function deleteOtherRevisions(string $id, string $vid, string $source_type, string $suffix = ''): void {
    $this->logger->debug('Deleting usage from non-current revisions: @type=@id, vid!=@vid', [
      '@type' => $source_type,
      '@id' => $id,
      '@vid' => $vid,
    ]);
    $this->database->delete('entity_usage')
      ->condition('source_type', $source_type)
      ->condition('source_id' . $suffix, $id)
      ->condition('source_vid' . $suffix, $vid, '<>')
      ->execute();
  }

  /**
   * Deletes records where the target is the same as the source.
   *
   * @todo Optimize these queries. They are not indexed and showed up in New Relic.
   */
  private function deleteRecordsPointingThemselves() {
    $this->database->query("
      DELETE FROM entity_usage
      WHERE target_id = source_id
    ")->execute();

    $this->database->query("
      DELETE FROM entity_usage
      WHERE target_id_string = source_id_string
    ")->execute();

  }

  /**
   * Returns records with repeated entity IDs.
   */
  private function getRepeatedRecords($field) {
    return $this->database->query("
        SELECT
          {$field}, source_type, COUNT({$field})
        FROM entity_usage
        GROUP BY {$field}, source_type HAVING COUNT({$field}) > 1
    ")->fetchAll();
  }

  /**
   * Deletes records with revisions that are not the current one.
   */
  private function deleteRecordsWithNonCurrentRevisions() {
    $res = [];
    $res[''] = $this->getRepeatedRecords('source_id');
    $res['_string'] = $this->getRepeatedRecords('source_id_string');

    foreach ($res as $key => $records) {

      foreach ($records as $record) {
        $source_type = $record->source_type;
        $source_id = $record->source_id;
        $def = $this->entityTypeManager->getDefinition($source_type);
        if (!$def->isRevisionable()) {
          continue;
        }
        $table = $def->getBaseTable();
        $key_eid = $def->getKey('id');
        $key_rid = $def->getKey('revision');
        $vid = $this->database->select($table, 'e')
          ->fields('e', [$key_rid])
          ->condition($key_eid, $source_id)
          ->execute()->fetchField();
        if ($vid) {
          $this->deleteOtherRevisions($source_id, $vid, $source_type, $key);
        }
      }
    }
  }

}
